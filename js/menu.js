main_controllers.controller('menu_controller', 
  function($scope, $rootScope, $location) {
  $scope.rs = $rootScope;

  // reset the entire setup and go to landings page
  $scope.logout = function () {

    $rootScope.session = {};
    $rootScope.ui_persons = {};
    $rootScope.ui_persons.list = {};
    $rootScope.ui_websites = {};
    $rootScope.ui_websites.list = {};

    $rootScope.ui_emails = {};
    $rootScope.ui_emails.list = {};






    $location.path('/landing');
  };


  $scope.landing_button = function () {
    $location.path('/landing');
  };

  $scope.landing1_button = function () {
    $location.path('/landing_authed');
  };

  $scope.login_button = function () {
    $location.path('/login');
  };

  $scope.persons_button = function () {
    $location.path('/persons');
  };

  $scope.websites_button = function () {
    $location.path('/websites');
  };

  $scope.emails_button = function () {
// sm("at emails_button in menu controller");

    $location.path('/emails');
  };

  $scope.pdfs_button = function () {
// sm("at emails_button in menu controller");

    $location.path('/pdfs');
  };

  $scope.register_button = function () {
    $location.path('/register');
  };

  $scope.add_websites_button = function () {
    $location.path('/websites_add');
  };

  $scope.add_email_button = function () {
    $location.path('/email_add');
  };

  $scope.add_pdf_button = function () {
    $location.path('/pdf_add');
  };

  $scope.profile_button = function (pid) {

   $rootScope.profile_view = true;  //    flag to determine the return route after view/edit
    $location.path('/person/' + pid);
  };

  $scope.edit_person_button = function (pid) {

   $rootScope.profile_view = true;  //    flag to determine the return route after view/edit
    $location.path('/person_edit/' + pid);
  };


  $scope.login_cancel_button = function () {
    $location.path('/landing');
  };

  $scope.contact_button = function () {
    console.log("At menu.js contact_button");
    $location.path('/contact');
  };


// ui_cancel_button
  // $scope.files_button = function () {
  //   $location.path('/files');
  // };

});