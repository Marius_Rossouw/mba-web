var am = angular.module('main_app', [
    'ui.bootstrap',
    'main_app.services',
    'main_app.controllers',
    'ngRoute',
    'ngGrid'
]);

// -----------------------------------------------------------------------------

am.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when("/landing", {templateUrl: "src/landing/landing.html", controller: "landing_controller"});
  $routeProvider.when("/landing_authed", {templateUrl: "src/landing/landing_authed.html", controller: "landing_controller"});

  $routeProvider.when("/login", {templateUrl: "src/login/login.html", controller: "login_controller"});

  $routeProvider.when("/register", {templateUrl: "src/register/register.html", controller: "register_controller"});
  $routeProvider.when("/register_confirmation", {templateUrl: "src/register/confirmation.html"});

  $routeProvider.when("/profile/:id", {templateUrl: "src/profile/profile.html", controller: "profile_controller"});
  $routeProvider.when("/profile_edit/:id", {templateUrl: "src/profile_edit/profile_edit.html", controller: "edit_profile_controller"});

  $routeProvider.when("/persons", {templateUrl: "src/persons/persons.html", controller: "persons_controller"});
  $routeProvider.when("/person/:id", {templateUrl: "src/persons/person.html", controller: "person_controller"});
  $routeProvider.when("/person_edit/:id", {templateUrl: "src/persons/person_edit.html", controller: "edit_person_controller"});

  $routeProvider.when("/websites", {templateUrl: "src/websites/websites.html", controller: "websites_controller"});
  $routeProvider.when("/website/:id", {templateUrl: "src/websites/website.html", controller: "website_controller"});
  $routeProvider.when("/website_edit/:id", {templateUrl: "src/websites/website_edit.html", controller: "edit_website_controller"});
  $routeProvider.when("/websites_add", {templateUrl: "src/websites/websites_add.html", controller: "add_websites_controller"});
  $routeProvider.when("/websites_add_confirmation", {templateUrl: "src/websites/confirm.html"});

  $routeProvider.when("/emails", {templateUrl: "src/emails/emails.html", controller: "emails_controller"});
  $routeProvider.when("/email/:id", {templateUrl: "src/emails/email.html", controller: "email_controller"});
  $routeProvider.when("/email_edit/:id", {templateUrl: "src/emails/email_edit.html", controller: "edit_email_controller"});
  $routeProvider.when("/email_add", {templateUrl: "src/emails/email_add.html", controller: "add_emails_controller"});
  $routeProvider.when("/email_add_confirmation", {templateUrl: "src/emails/email_confirm.html"});

  $routeProvider.when("/pdfs", {templateUrl: "src/pdfs/pdfs.html", controller: "pdfs_controller"});
  $routeProvider.when("/pdf/:id", {templateUrl: "src/pdfs/pdf.html", controller: "pdf_controller"});
  $routeProvider.when("/pdf_edit/:id", {templateUrl: "src/pdfs/pdf_edit.html", controller: "edit_pdf_controller"});
  $routeProvider.when("/pdf_add", {templateUrl: "src/pdfs/pdf_add.html", controller: "add_pdfs_controller"});
  $routeProvider.when("/pdf_add_confirmation", {templateUrl: "src/pdfs/pdf_confirm.html"});

  $routeProvider.when("/contact", {templateUrl: "src/contact/contact.html", controller: "contact_controller"});
  $routeProvider.when("/contact_confirmation", {templateUrl: "src/contact/contact_confirm.html"});


  $routeProvider.otherwise({redirectTo: '/landing'});
}]);

// ---------------------------------------------------------------------------


// var main_services = angular.module('main_app.services', []);
// var main_controllers = angular.module('main_app.controllers', []);

// // Curious thing: here curUse and purr can be defined with or 
// // without the var, it is accepted in the global namespace all the same.
// curUse="curUse";
// purr="purr";
// bsm=true;
// ---------------------------------------------------------------------------

am.run(function($rootScope, init_service) {
  console.log("run");

  $rootScope.lookups = {};
  $rootScope.api = {};

  $rootScope.login = {result : { token:''}};
  $rootScope.session = {};

// Define a flag in the global namespace

  $rootScope.profile_view = true;   //  set the view flag for individual view to true (default)
                            // This is a flag to determine the return route after viewing a user's profile
                            // If the "View [user] profile option at the landing_authed page
                            // has been clicked , it is also set to true in menu.js. 
                            // If the "view user" option is selected 
                            // from the Users view, it is set to false in menu.js
                            
  $rootScope.su = false;   
                                  // set superuser to false and change to true if
                                  // a user with superuser privileged signs in
  
  


  init_service.init_api_uris();
  init_service.init_lookup_selectors();
});
//-------------------------------------------------------------------

am.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    // scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    scope.fileread = changeEvent.target.files;
                });
            });
        }
    };
}]);

// ------------------------------------------------------------------


var main_services = angular.module('main_app.services', []);
var main_controllers = angular.module('main_app.controllers', []);
// ------------------------------------------------------

/// define a global boolean which if true
// will contain the value true if the logged in user is a superuser.
// This is a workaround a problem that $rootScope.superuser does not retains its value on the view
Spr=false
// --------------------------------------------------------
// define a global boolean which if true
// will show alert messages invoked for example as follows:

// sm("This is a popup messages")

// NB: sm("") will not bring up the alert window
bsm=true;
// var bsm=false;
// ------------------------------------------------------------
// Define a global email placeholder to carry the email address of 
// the signed in user so it can be used in all modules

var LoginEmail = 'Not logged in';
