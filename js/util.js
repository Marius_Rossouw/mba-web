// General utilities

// Remove the active attribute from a menu item
function navbar_remove_all_active() {
  $('#content_navbar li').each(function() {
    $("#"+this.id).removeClass("active");
  });
}

// Show a message - bsm (boolean show message) must be
// set to true in init.js for the message(s) to appear
function sm(sin) {

  if (!sin == ""){
    if (bsm) {
    alert("> "+sin);
  };
  };
}
