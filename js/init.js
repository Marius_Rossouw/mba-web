// ------------ init.js -----------
//         

main_services.factory('init_service', function($rootScope) {
  var service_functions = {};

  service_functions.init_api_uris = function() {
    $rootScope.api_mba = 'http://localhost:3013';
    // $rootScope.api_mba = 'http://memberbase.net:3013';
  };

  service_functions.init_lookup_selectors = function() {

  };

  return service_functions;
});


// define a global boolean which if true
// will show alert messages invoked for example as follows:

// sm("This is a popup messages")

// NB: sm("") will not bring up the alert window
// var bsm=true;
// var bsm=false;

// The curious thing is that if bsm is defined here in init
// you need the var .  If it is defined in app.js in the run module, 
// you must not use the var
// Go figure

 // var curUse="curUse";
// var purr="purr";
  console.log("init");



