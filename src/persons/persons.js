/*  -------  persons.js  ---------- 
      
*/
          main_controllers.controller('persons_controller', 
            function($http, $scope, $rootScope, $location) {

 navbar_remove_all_active();
 $("#navli_persons").addClass("active");

 $scope.rs = $rootScope;
 $scope.ui = {};

 if (!$scope.rs.ui_persons){
  $scope.rs.ui_persons = {};
  $scope.rs.ui_persons.list = [];
}

$scope.ui_search_persons_button = function () {

    // Get the data from the databaae at host api-mba  eg. localhost:3013
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/persons',
      params: { 'foobar': new Date().getTime() }
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.rs.ui_persons.list = data;
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in web persons.js \n errID-42: ' + data);
      }
    }
  };


//create the arrow button that views the individual person details
var grid_view_person_template = '<div>';
grid_view_person_template += '<button type="button" class="btn btn-xs"';
grid_view_person_template += 'title="View the complete profile of this user"';
grid_view_person_template += 'ng-click="view_person(row.entity[col.field])"';


// The following if procedure is a symptomatic relief (workaround) and
// was implemented becasue in building the below template,
// the value of su is not carried over and becomes undefined for some obscure reason

// to hide the View button, for instance, if not a superuser, use the following:
// if(su){  
//   grid_view_person_template += 'ng-show="true">';
// } else { 
//   grid_view_person_template += 'ng-show="false">';
// }


if($rootScope.su){  // true

  grid_view_person_template += 'ng-disabled="false">';
} else {  // false
  grid_view_person_template += 'ng-disabled="true">';
}

grid_view_person_template += '<i class="fa fa-arrow-right"></i>';
grid_view_person_template += '</button>';
grid_view_person_template += '</div>';

var grid_delete_person_template = '<div>';
grid_delete_person_template += '<button type="button" class="btn btn-xs"';
grid_delete_person_template += 'title="Delete this user from the database"';
grid_delete_person_template += 'ng-click="delete_person(row.entity[col.field])"';

if($rootScope.su){  // true
  grid_delete_person_template += 'ng-disabled="false">';
} else {  // false
  grid_delete_person_template += 'ng-disabled="true">';
}

grid_delete_person_template += '<i class="fa fa-trash-o"></i>';
grid_delete_person_template += '</button>';
grid_delete_person_template += '</div>';

var grid_edit_person_template = '<div>';
grid_edit_person_template += '<button type="button" class="btn btn-xs"';
grid_edit_person_template += 'title="Edit the profile of this user"';
grid_edit_person_template += ' ng-click="edit_person_icon(row.entity[col.field])"';

if($rootScope.su){grid_edit_person_template += 'ng-disabled="false">';}   // true
else {grid_edit_person_template += 'ng-disabled="true">';}  // false

grid_edit_person_template += '<i class="fa fa-pencil"></i>';
grid_edit_person_template += '</button>';
grid_edit_person_template += '</div>';
 
  $scope.view_person = function(pid){


    $rootScope.profile_view = false
    $location.path('/person/' + pid );
  };

  $scope.edit_person_icon = function(pid){
    $rootScope.profile_view = false
    $location.path('/person_edit/' + pid);
  };

  $scope.grid_cols_def = [
  {field: '_id', displayName: 'View', width: 50, cellTemplate : grid_view_person_template, editableCellTemplate : grid_view_person_template },
  {field: '_id', displayName: 'Del', width: 45, cellTemplate : grid_delete_person_template, editableCellTemplate : grid_delete_person_template },
  {field: '_id', displayName: 'Edit', width: 45, cellTemplate : grid_edit_person_template, editableCellTemplate : grid_edit_person_template },
  {field: 'name', displayName: 'Name', width: 200},
  {field: 'email', displayName: 'Email', width: 200}
  ];

//If you do not want to show the View, Delete and Edit buttons on the grid, use the following instead
// if(su){
//   $scope.grid_cols_def = [
//   {field: '_id', displayName: 'View', width: 50, cellTemplate : grid_view_person_template, editableCellTemplate : grid_view_person_template },
//   {field: '_id', displayName: 'Del', width: 45, cellTemplate : grid_delete_person_template, editableCellTemplate : grid_delete_person_template },
//   {field: '_id', displayName: 'Edit', width: 45, cellTemplate : grid_edit_person_template, editableCellTemplate : grid_edit_person_template },
//   {field: 'name', displayName: 'Name', width: 200},
//   {field: 'email', displayName: 'Email', width: 200}
//   ];
// }
// else {
//   $scope.grid_cols_def = [
//   {field: 'name', displayName: 'Name', width: 200},
//   {field: 'email', displayName: 'Email', width: 200}
//   ]};

  $scope.grid_filter_options = {
    filterText: ''
  };

  $scope.grid_options = {
    data: 'rs.ui_persons.list',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def',
    enablePaging: true,
    filterOptions: $scope.grid_filter_options
  };

  $scope.delete_person = function(pid){

if(confirm("Are you sure you want to delete this user?") == false) {
  return;
}

    var mypromise =  $http({
      method: 'DELETE',
      url: $rootScope.api_mba + '/persons/' + pid,
      params: { 'foobar': new Date().getTime() }
    });

    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui_search_persons_button();
      } else {
        http_error(data);
      }
    });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };
});

// ----------------------------------------------------
main_controllers.controller('person_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_profile").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};

  $scope.ui_get_person_button = function () {
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/persons/' + $routeParams.id,
      params: { 'foobar': new Date().getTime() }
    });

    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
      } else {
        http_error(data);
      }
    });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error: ' + data);
      }
    }
  };

  $scope.ui_get_person_button();

  $scope.ui_edit_person_button = function(){

    if ($scope.ui._id){
      $location.path('/person_edit/' + $scope.ui._id);
    }
  };

// Returning from View Profile of logged in person
$scope.view_profile_return = function(){
  if ($rootScope.profile_view){
    $location.path('/landing_authed');
  } else {
    $location.path('/persons');
  }
};
});

// ------------------------------------------------------------------
main_controllers.controller('edit_person_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();

   if($rootScope.profile_view){
  $("#navli_profile").addClass("active");
  } else {
  $("#navli_persons").addClass("active");
    // $location.path('/persons');
  }



  
  // $("#navli_persons").addClass("active");
  $scope.uidirty = false;
  $scope.ui = {};
  
  get_data();
  function get_data(){
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/persons/' + $routeParams.id,  
      params: { 'foobar': new Date().getTime() }
    });

    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
      } else {
        get_error(data);
      }
    });

    mypromise.error(function(data, status, headers, config) {
      get_error(data);
    });
    function get_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in person_edit \nerrID-272: ' + data);
      }
    }
  };

  $scope.ui_edit_person_cancel = function () {
   if($rootScope.profile_view){
    $location.path('/landing_authed');
  } else {
    $location.path('/persons');
  }
};

  $scope.ui_edit_person_clear = function () {

    $scope.ui.firstname = '';
    $scope.ui.lastname = '';
    $scope.ui.email = '';
    $scope.ui.password = '';
    $scope.ui.superuser = '';
    $location.path('/person_edit/' + $scope.ui._id);
  };

$scope.ui_edit_person_submit = function (formvalid) {
    var mypromise =  $http({
      method: 'PUT',
      url: $rootScope.api_mba + '/persons/' + $routeParams.id, //map to API server.js: app.put('/websites', websites.websites_update_one); 
      params: { 'foobar': new Date().getTime() },
      data: $scope.ui
    });

    mypromise.success(function(data, status, headers, config) {
      console.log('Person update success ',status);
      if (status == 200) {                    // then a;; id okay and we're done
        alert("Person updated successfully"); // so inform the user

        if($rootScope.profile_view){          // is true, it means we came from the main menu
          $location.path('/landing_authed');  // so go back there
        } else {                              // otherwise
          $location.path('/persons');         // go back to the persons view
        }
      } else {
        update_error(data);
      }
    });

    mypromise.error(function(data, status, headers, config) {
      update_error(data);
    });
    function update_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in persons.js near line ~340:\n ' + data);
      }
    }
  };
});
