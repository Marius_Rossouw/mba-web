 // ----- login.js ------

function clear_session(rs){
  rs.token = "";
  rs.username = "";
  rs.su = "";        // clear superuser
}


main_controllers.controller('login_controller', function($http, $scope, $rootScope, $location) {

 navbar_remove_all_active();
 $("#navli_login").addClass("active");

 if (!$rootScope.session) {
  setup_auth_token($rootScope);
}

$scope.ui = {};

// Shortcut login for admin without suoeruser privilages
$scope.ui_login_submit_admin = function () {

  $scope.ui.email = 'ad@min.com';
  $scope.ui.password = 'admin';
  $scope.ui_login_submit();
};

// Shortcut login for superuser with suoeruser privilages
$scope.ui_login_submit_super = function () {
  $scope.ui.email = 'super@user.com';
  $scope.ui.password = 'super';
  $scope.ui_login_submit();
};

$scope.login_cancel_button = function () {
  $location.path('/landing');
};

$scope.login_clear_button = function () {
  $scope.ui.email='';
  $scope.ui.password='';
  $location.path('/login');
};

$scope.ui_login_submit = function () {

  var mypromise =  $http({
    method: 'POST',
    url: $rootScope.api_mba + '/login',
    params: {
      'foobar': new Date().getTime()
    },
    data: {
      'auth_identifier':$scope.ui.email,
      'auth_password':$scope.ui.password
    }});

  mypromise.success(function(data, status, headers, config) {
    // sm("success data="+data);
    // sm("status="+status);
    // sm("header="+header);
    // sm("config="+config); 
    if (status == 201) {
      $rootScope.session.authed = true;
      $rootScope.session.token = data.token;
      $rootScope.session.username = data.username;
      $rootScope.session.person_id = data.person_id;  // this is the same as token; why?
      $rootScope.session.email = data.email;
      $rootScope.su = (data.superuser == true || data.superuser == "true");
      // for some reason using $rootScope.su does not work at the views
      LoginEmail = data.email;  // used for identifying the owner of a website

      // sm("At login ui_login_submit: typeof($rootScope.su)= "   
      //   + typeof($rootScope.su) +"\n$rootScope.su=" + $rootScope.su
      //   + "\n data.username-" + data.username
      //   + "\n data.superuser-" + data.superuser);

  $location.path('/landing_authed');
} else {
  http_error(data);
}

});

    // sm("data="+data);
    // sm("status="+status);
    // sm("header="+header);
    // sm("config="+config); 
  mypromise.error(function(data, status, headers, config) {
    // sm("error data="+data);
    // sm("status="+status);
    // sm("header="+header);
    // sm("config="+config); 

    http_error(data);
  });

  function http_error(data){
    if (data.message) {
/*    sm("error2 data="+data);
    sm("status="+status);
    sm("header="+header);
    sm("config="+config); 
*/
      alert(data.message);
    } else {

    //       sm("error3 data="+data);
    // sm("status="+status);
    // sm("header="+header);
    // sm("config="+config); 
      alert('Unknown error in login.js errID-101 \nCheck if the server is running.' + data);
    }
  }
}
});
