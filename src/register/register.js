// ------ register.js ---------
// api_mba

main_controllers.controller('register_controller', function($scope, $rootScope, $http, $location) {
  navbar_remove_all_active();
  $("#navli_register").addClass("active");

  // navbar_remove_all_active();
  // $("#navli_persons").addClass("active");

  $scope.ui = {};
  $scope.uidirty = false;

  $scope.rs = $rootScope;

  $scope.$watch("ui",function() {
    var custom_valid = validate_form($scope, $rootScope);
  },true);


// Returning from Registering new Profile
$scope.reg_cancel_btn = function(){

  $location.path('/landing');
  
};

// clear all input fields on the registration form
$scope.reg_clear_btn = function(){  
  $scope.ui.firstname='';
  $scope.ui.lastname='';
  $scope.ui.email='';
  $scope.ui.password='';
  $scope.ui.accept_tac=false;

  $location.path('/register');  // reload the registration form
};

// Submit the registration form
    $scope.reg_submit_btn = function (formvalid) {
    if (!formvalid) {
      $scope.uidirty = true;
      alert('Please check, some entries are not valid.');
      return;
    }
    if (!validate_form($scope, $rootScope)) {
      $scope.uidirty = true;
      alert('Please check, some entries are invalid.');
      return;
    }

    var payload = {};
    payload.firstname = $scope.ui.firstname;
    payload.lastname = $scope.ui.lastname;
    payload.email = $scope.ui.email;
    payload.password = $scope.ui.password;
    payload.superuser = false;

    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_mba + '/register',
      params: { 'foobar': new Date().getTime(),'dum1': 'dum-1', 'dum2': 'dum-2' },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 201) {
        $location.path('/register_confirmation');
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in register.js ErrID-reg101: ' + data);
      }
    }
  };

  function validate_form($scope, $rootScope){
    var validate_ok = true;
    $scope.ui.invalid = {};
    
    if (!$scope.ui.accept_tac) {
      $scope.ui.invalid.accept_tac = true;
      validate_ok = false;
    }
    return validate_ok;
  }
});
