/* ---------- websites.js ----------
       
This is the controller that controls the functions within the websites.html file
These functions include 
- ui_get_websites_button()
- view_website
- delete_website
- edit_website_icon

The sequence of variables are as follows for consistence:
// webName
// webOwner
// webUrl
// webCat
// webRepo
// apiRepo
// webSvrAdr
// apiSvrAdr
// apiPort

*/
main_controllers.controller('websites_controller', function($http, $scope, $rootScope, $location) {
  navbar_remove_all_active();
  $("#navli_websites").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};

  if (!$scope.rs.ui_websites){
    $scope.rs.ui_websites = {};
    $scope.rs.ui_websites.list = [];
  }

  $scope.ui.webOwner= LoginEmail;

  /* --------------------------------------------------------------------
  A function that uses GET and links to the backend 
  with url: $rootScope.api_mba + '/websites', and 
  then places the recieved data into the rootScope 
  */
  $scope.ui_get_websites_button = function () {

// sm("LoginEmail=" +  LoginEmail +" " + $rootScope.su);

var mypromise =  $http({
  method: 'GET',
        //map to API server.js: app.get('/websites', websites.websites_list);
        url: $rootScope.api_mba + '/websites',    
        params: {
          'foobar': new Date().getTime(),
          'spr': $rootScope.su,
          'em': LoginEmail
        },
      });
mypromise.success(function(data, status, headers, config) {

  if (status == 200) {
    if(data == "" || data == null || data== undefined){
      alert("No websites found for \n" + LoginEmail);
      return;
    }


    /* place the data into the rootScope(rs) */
    $scope.rs.ui_websites.list = data;         
  } else {
    http_error(data);
  }
});
mypromise.error(function(data, status, headers, config) {
  /* sm("error Object.keys=" + Object.keys(data)); */

  http_error(data);
});
function http_error(data){
  if (data.message) {
    alert(data.message);
  } else {
    alert('Unknown error in web websites.js\n errID-80): ' + data);
  }
}
};

//create the arrow button that opens the individual website
var grid_view_web_template = '<div>';
grid_view_web_template += '<button type="button" class="btn btn-xs"';
grid_view_web_template += 'title="View the details of this website"';
grid_view_web_template += 'ng-click="view_website(row.entity[col.field])">';  
// bevat die _id, word gebruik as pid 
// e.g. $scope.view_website = function(pid){
  grid_view_web_template += '<i class="fa fa-arrow-right"></i>';
  grid_view_web_template += '</button>';
  grid_view_web_template += '</div>';

  //create the bin button that deletes the individual website
  var grid_delete_web_template = '<div>';
  grid_delete_web_template += '<button type="button" class="btn btn-xs" ';
  grid_delete_web_template += ' title="Delete this website from the website database" ';
  grid_delete_web_template += 'ng-click="delete_website(row.entity[col.field])">';
  grid_delete_web_template += '<i class="fa fa-trash-o"></i>';
  grid_delete_web_template += '</button>';
  grid_delete_web_template += '</div>';

  var grid_edit_web_template = '<div>';
  grid_edit_web_template += '<button type="button" class="btn btn-xs" ';
  grid_edit_web_template += 'title="Edit the details of this website" ';
  grid_edit_web_template += 'ng-click="edit_website_icon(row.entity[col.field])">';
  grid_edit_web_template += '<i class="fa fa-pencil"></i>';
  grid_edit_web_template += '</button>';
  grid_edit_web_template += '</div>';
  
  /*
  A function that redirects the browser to view the selected website
  */
  $scope.view_website = function(pid){  
   // the pid is derived from the definition 
  // of the grid_view_template above

  $location.path('/website/' + pid );
};

  /*
  A function that redirects the browser to edit the selected website
  */
  $scope.edit_website_icon = function(pid){
  // console.log(pid);
  $location.path('/website_edit/' + pid);
};

//builds the grid within which the websites will appear as a list
$scope.grid_cols_def = [
{field: '_id', displayName: 'Open', width: 50, 
cellTemplate : grid_view_web_template, 
editableCellTemplate : grid_view_web_template },
{field: '_id', displayName: 'Del', width: 35, 
cellTemplate : grid_delete_web_template, 
editableCellTemplate : grid_delete_web_template },
{field: '_id', displayName: 'Edit', width: 40, 
cellTemplate : grid_edit_web_template, 
editableCellTemplate : grid_edit_web_template },
{field: 'webName', displayName: 'Website name', width: 130},
{field: 'webOwner', displayName: 'Owner', width: 100} ,
{field: 'webUrl', displayName: 'Website Url', width: 130},
{field: 'webCat', displayName: 'Category', width: 100} ,
{field: 'webRepo', displayName: 'Web Repo', width: 150},
{field: 'apiRepo', displayName: 'Api Repo', width: 150},
{field: 'webSvrAdr', displayName: 'Web Server address', width: 200},
{field: 'apiSvrAdr', displayName: 'Api Server address', width: 200},
{field: 'apiPort', displayName: 'Api port', width: 80}

];

$scope.grid_filter_options = {
  filterText: ''
};

//Setting for the grid that contains the websites
$scope.grid_options = {
  data: 'rs.ui_websites.list',
  enableCellSelection: false,
  enableRowSelection: false,
  enableCellEditOnFocus: true,
  columnDefs: 'grid_cols_def',
  enablePaging: true,
  filterOptions: $scope.grid_filter_options
};

  /*
  A function that uses DELETE and links to the backend 
  with url: $rootScope.api_mba + '/websites/' + pid, and 
  after deleting the selected website it recalls the 
  ui_get_websites_button() function to reload the list of 
  websites in the grid
  */
  $scope.delete_website = function(pid){

    if(confirm("Are you sure you want to delete this website?") == false) {
      return;
    }


    var mypromise =  $http({
      method: 'DELETE',
        url: $rootScope.api_mba + '/websites/' + pid,    //map to API server.js: app.delete('/websites/:id', websites.websites_delete_one);
        params: {
          'foobar': new Date().getTime()
        }
      });

    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
          $scope.ui_get_websites_button();       // Recalls the ui_get_websites_button() function to reload the list of scchools in the grid
        } else {
          http_error(data);
        }
      });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });

    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in web websites.js \nerrID-207: ' + data);
      }
    }
  };
});


/*

*/
main_controllers.controller('website_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_websites").addClass("active");
  $scope.rs = $rootScope;
  $scope.ui = {};

  /*

  */
  $scope.ui_get_website_button = function () {
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/websites/' + $routeParams.id,
      params: {
        'foobar': new Date().getTime()
      }

    });



    mypromise.success(function(data, status, headers, config) {
        // sm("success Object.keys=" + Object.keys(data));

        if (status == 200) {


          $scope.ui = data;
        } else {
          http_error(data);
        }
      });

    mypromise.error(function(data, status, headers, config) {

        // sm("error1 Object.keys=" + Object.keys(data));


        http_error(data);
      });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error  in web websites.js (~190): ' + data);
      }
    }
  };

  $scope.ui_get_website_button();

  /*

  */
  $scope.ui_edit_website_button = function(){

    if ($scope.ui._id){
      $location.path('/website_edit/' + $scope.ui._id);
    }
  };


  $scope.website_view_cancel = function () {
    // sm("at website_view_cancel");
    $location.path('/websites');
  };

});    // end website_controller


/*

*/
main_controllers.controller('add_websites_controller', function($scope, $rootScope, $http, $location) {
 // sm("At add_websites_controller");

 navbar_remove_all_active();
 $("#navli_add_websites").addClass("active");

 $scope.ui = {};
 $scope.ui.webOwner = LoginEmail;
 $scope.uidirty = false;

 $scope.rs = $rootScope;

 $scope.$watch("ui",function() {
  var custom_valid = validate_form($scope, $rootScope);
},true);

  // Cancel adding a new website
  $scope.web_add_cancel = function () {
    $location.path('/landing_authed');
  };


  //  - clear all fileds except the  owner email
  $scope.web_add_clear = function () {

    $scope.ui.webName = '';
    // $scope.ui.apiOwner = '';  // we keep the owner along with his _id
    $scope.ui.webUrl = '';
    $scope.ui.webCat = '';
    $scope.ui.webRepo = '';
    $scope.ui.apiRepo = '';
    $scope.ui.webSvrAdr = '';
    $scope.ui.apiSvrAdr = '';
    $scope.ui.apiPort = '';

    $location.path('/websites_add');
  };

  /*
   Create a test set for quick population of the fields within the document
   */
   $scope.web_add_populate = function () {

// Generate a random number to append to the data
var rn = Math.floor((Math.random() *1000) + 1);
var rns = rn.toString();

$scope.ui.webName = 'webName-' + rns;
$scope.ui.webOwner = LoginEmail;
$scope.ui.webUrl = 'webUrl-' + rns;
$scope.ui.webCat = 'webCat-' + rns;
$scope.ui.webRepo = 'webRepo-' + rns;
$scope.ui.apiRepo = 'apiRepo-' + rns;
$scope.ui.webSvrAdr = 'webSvrAdr-' + rns;
$scope.ui.apiSvrAdr = 'apiSvrAdr-' + rns;
$scope.ui.apiPort = rns + "0";

$location.path('/websites_add');

};

$scope.web_add_submit = function (formvalid) {
  if (!formvalid) {
    $scope.uidirty = true;
    alert('Please check, some entries are not valid. ');
    return;
  }

  var payload = {};

  payload.webName = $scope.ui.webName;
  payload.webOwner = $scope.ui.webOwner;
  payload.webUrl = $scope.ui.webUrl;  
  payload.webCat = $scope.ui.webCat;
  payload.webRepo = $scope.ui.webRepo;
  payload.apiRepo = $scope.ui.apiRepo;
  payload.webSvrAdr = $scope.ui.webSvrAdr;
  payload.apiSvrAdr = $scope.ui.apiSvrAdr;
  payload.apiPort = $scope.ui.apiPort;

  var mypromise =  $http({
    method: 'POST',
      url: $rootScope.api_mba + '/websites',   //map to API server.js: app.post('/websites', websites.websites_add_one); 
      params: { 'foobar': new Date().getTime() },
      data: payload
    });

  mypromise.success(function(data, status, headers, config) {
    if (status == 201) {
      $location.path('/websites_add_confirmation');
    } else {
      http_error(data);
    }
  });

  mypromise.error(function(data, status, headers, config) {
    http_error(data);
  });

  function http_error(data){
    if (data.message) {
      alert(">>data.message= " + data.message);
    } else {
      alert('Unknown error in web websites.js (~305): ' + data);
    }
  }
};

  /*

  */
  function validate_form($scope, $rootScope){
    var validate_ok = true;
    $scope.ui.invalid = {};
    
    if (!$scope.ui.accept_tac) {
      $scope.ui.invalid.accept_tac = true;
      validate_ok = false;
    }
    return validate_ok;
  }
});

/*

*/
main_controllers.controller('edit_website_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_websites").addClass("active");

  $scope.uidirty = false;
  $scope.ui = {};
  //$scope.uilookup = {};

  get_data();


  /*

  */
  function get_data(){
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/websites/' + $routeParams.id, //map to API server.js: app.get('/websites', websites.websites_get_one); 
      params: { 'foobar': new Date().getTime() }
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
      } else {
        get_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      get_error(data);
    });
    function get_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in web websites.js (~364): Data:' + data + "  Status:" + status + "  Headers:" + "  Config:" + config);
      }
    }
  }

  $scope.web_edit_cancel = function () {

  // show_image('images/wait.gif', 30, 30, 'Wait');
  // sm("at web_edit_cancel");
  $location.path('/websites');
};

  //  - clear all fields except the  owner email
  $scope.web_edit_clear = function () {

    $scope.ui.webName = '';
    // $scope.ui.apiOwner = '';  // we keep the owner along with his _id
    $scope.ui.webUrl = '';
    $scope.ui.webCat = '';
    $scope.ui.webRepo = '';
    $scope.ui.apiRepo = '';
    $scope.ui.webSvrAdr = '';
    $scope.ui.apiSvrAdr = '';
    $scope.ui.apiPort = '';
  };

  $scope.web_edit_submit = function (formvalid) {
    /*
    // The following procedure was commented out for some reason
   //  if (!formvalid) {
   //   $scope.uidirty = true;
   //   alert("uidirty= " + uidirty)
   //   return;
   // }
   */

   var mypromise =  $http({
    method: 'PUT',
      url: $rootScope.api_mba + '/websites/' + $routeParams.id, //map to API server.js: app.put('/websites', websites.websites_update_one); 
      params: { 'foobar': new Date().getTime() },
      data: $scope.ui
    });

// sm(Object.keys($routeParams));

mypromise.success(function(data, status, headers, config) {
  console.log('website update success ',status);
  if (status == 200) {
    alert("Website updated successfully");

    $location.path('/websites');



  } else {
    update_error(data);
  }
});

mypromise.error(function(data, status, headers, config) {
  update_error(data);
});
function update_error(data){
  if (data.message) {
    alert(data.message);
  } else {
    alert('Unknown error in web websites.js (~410): ' + data);
  }
}
};
});

function show_image(src, width, height, alt) {
  var img = document.createElement("img");
  img.src = src;
  img.width = width;
  img.height = height;
  img.alt = alt;

    // This next line will just add it to the <body> tag
    document.body.appendChild(img);
  }