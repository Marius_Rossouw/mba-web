/* ---------- pdfs.js ----------
       
This is the controller that controls the functions within the pdfs.html file
These functions include 
- ui_get_pdfs_button()
- view_pdf
- delete_pdf
- edit_pdf_icon

The sequence of variables are as follows for consistency:

et_name
et_created_by
et_updated_by
et_product_name
et_website_name  (dropdown)
et_subject
et_from_address  
et_smtp_id   (dropdown)
et_body



*/
main_controllers.controller('pdfs_controller', function($http, $scope, $rootScope, $location) {
  navbar_remove_all_active();
  $("#navli_pdfs").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};

  if (!$scope.rs.ui_pdfs){
    $scope.rs.ui_pdfs = {};
    $scope.rs.ui_pdfs.list = [];
  }

  $scope.ui.pdf_Owner= LoginEmail;

  /* --------------------------------------------------------------------
  A function that uses GET and links to the backend 
  with url: $rootScope.api_mba + '/pdfs', and 
  then places the recieved data into the rootScope 
  */
  $scope.ui_get_pdfs_button = function () {

// sm("LoginEmail=" +  LoginEmail +" " + $rootScope.su);

var mypromise =  $http({
  method: 'GET',
        //map to API server.js: app.get('/pdfs', pdfs.pdfs_list);
        url: $rootScope.api_mba + '/pdfs',    
        params: {
          'foobar': new Date().getTime(),
          'spr': $rootScope.su,
          'em': LoginEmail
        },
      });
mypromise.success(function(data, status, headers, config) {

  if (status == 200) {
    if(data == "" || data == null || data== undefined){
      alert("No pdfs found for \n" + LoginEmail);
      return;
    }


    /* place the data into the rootScope(rs) */
    $scope.rs.ui_pdfs.list = data;         
  } else {
    http_error(data);
  }
});
mypromise.error(function(data, status, headers, config) {
  /* sm("error Object.keys=" + Object.keys(data)); */

  http_error(data);
});
function http_error(data){
  if (data.message) {
    alert(data.message);
  } else {
    alert('Unknown error in pdf_ pdfs.js\n errID-80): ' + data);
  }
}
};

//create the arrow button that opens the individual pdf
var grid_view_pdf_template = '<div>';
grid_view_pdf_template += '<button type="button" class="btn btn-xs"';
grid_view_pdf_template += 'title="View the details of this pdf"';
grid_view_pdf_template += 'ng-click="view_pdf(row.entity[col.field])">';  
// bevat die _id, word gebruik as pid 
// e.g. $scope.view_pdf = function(pid){
  grid_view_pdf_template += '<i class="fa fa-arrow-right"></i>';
  grid_view_pdf_template += '</button>';
  grid_view_pdf_template += '</div>';

  //create the bin button that deletes the individual pdf
  var grid_delete_pdf_template = '<div>';
  grid_delete_pdf_template += '<button type="button" class="btn btn-xs" ';
  grid_delete_pdf_template += ' title="Delete this pdf from the pdf database" ';
  grid_delete_pdf_template += 'ng-click="delete_pdf(row.entity[col.field])">';
  grid_delete_pdf_template += '<i class="fa fa-trash-o"></i>';
  grid_delete_pdf_template += '</button>';
  grid_delete_pdf_template += '</div>';

  var grid_edit_pdf_template = '<div>';
  grid_edit_pdf_template += '<button type="button" class="btn btn-xs" ';
  grid_edit_pdf_template += 'title="Edit the details of this pdf" ';
  grid_edit_pdf_template += 'ng-click="edit_pdf_icon(row.entity[col.field])">';
  grid_edit_pdf_template += '<i class="fa fa-pencil"></i>';
  grid_edit_pdf_template += '</button>';
  grid_edit_pdf_template += '</div>';
  
  /*
  A function that redirects the browser to view the selected pdf
  */
  $scope.view_pdf = function(pid){  
   // the pid is derived from the definition 
  // of the grid_view_template above

  $location.path('/pdf/' + pid );
};

  /*
  A function that redirects the browser to edit the selected pdf
  */
  $scope.edit_pdf_icon = function(pid){
  // console.log(pid);
  $location.path('/pdf_edit/' + pid);
};

//builds the grid within which the pdfs will appear as a list
$scope.grid_cols_def = [
{field: '_id', displayName: 'Open', width: 50, 
cellTemplate : grid_view_pdf_template, 
editableCellTemplate : grid_view_pdf_template },
{field: '_id', displayName: 'Del', width: 35, 
cellTemplate : grid_delete_pdf_template, 
editableCellTemplate : grid_delete_pdf_template },
{field: '_id', displayName: 'Edit', width: 40, 
cellTemplate : grid_edit_pdf_template, 
editableCellTemplate : grid_edit_pdf_template },
{field: 'et_name', displayName: 'pdf name', width: 130},
{field: 'et_product_name', displayName: 'Product name', width: 130},
{field: 'et_website_name', displayName: 'Website name', width: 100} ,
{field: 'et_subject', displayName: 'pdf subject', width: 150},
{field: 'et_from_address', displayName: 'pdf From affress', width: 150},
{field: 'et_smtp_id', displayName: 'E smtp_id', width: 200},
{field: 'et_body', displayName: 'pdf body', width: 80},
{field: 'et_creatred_by', displayName: 'Created By', width: 200},
{field: 'create_date', displayName: 'Create Date', width: 80}

];

// et_name
// et_created_by
// et_updated_by
// et_product_name
// et_website_name  (dropdown)
// et_subject
// et_from_address  
// et_smtp_id   (dropdown)
// et_body



$scope.grid_filter_options = {
  filterText: ''
};

//Setting for the grid that contains the pdfs
$scope.grid_options = {
  data: 'rs.ui_pdfs.list',
  enableCellSelection: false,
  enableRowSelection: false,
  enableCellEditOnFocus: true,
  columnDefs: 'grid_cols_def',
  enablePaging: true,
  filterOptions: $scope.grid_filter_options
};

  /*
  A function that uses DELETE and links to the backend 
  with url: $rootScope.api_mba + '/pdfs/' + pid, and 
  after deleting the selected pdf it recalls the 
  ui_get_pdfs_button() function to reload the list of 
  pdfs in the grid
  */
  $scope.delete_pdf = function(pid){

    if(confirm("Are you sure you want to delete this pdf?") == false) {
      return;
    }


    var mypromise =  $http({
      method: 'DELETE',
        url: $rootScope.api_mba + '/pdfs/' + pid,    //map to API server.js: app.delete('/pdfs/:id', pdfs.pdfs_delete_one);
        params: {
          'foobar': new Date().getTime()
        }
      });

    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
          $scope.ui_get_pdfs_button();       // Recalls the ui_get_pdfs_button() function to reload the list of scchools in the grid
        } else {
          http_error(data);
        }
      });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });

    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in pdf_ pdfs.js \nerrID-207: ' + data);
      }
    }
  };
});


/*

*/
main_controllers.controller('pdf_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_pdfs").addClass("active");
  $scope.rs = $rootScope;
  $scope.ui = {};

  /*

  */
  $scope.ui_get_pdf_button = function () {
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/pdfs/' + $routeParams.id,
      params: {
        'foobar': new Date().getTime()
      }

    });



    mypromise.success(function(data, status, headers, config) {
        // sm("success Object.keys=" + Object.keys(data));

        if (status == 200) {


          $scope.ui = data;
        } else {
          http_error(data);
        }
      });

    mypromise.error(function(data, status, headers, config) {

        // sm("error1 Object.keys=" + Object.keys(data));


        http_error(data);
      });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error  in  pdfs.js errID-274: ' + data);
      }
    }
  };

  $scope.ui_get_pdf_button();

  /*

  */
  $scope.ui_edit_pdf_button = function(){

    if ($scope.ui._id){
      $location.path('/pdf_edit/' + $scope.ui._id);
    }
  };


  $scope.pdf_view_cancel = function () {
    // sm("at pdf_view_cancel");
    $location.path('/pdfs');
  };

});    // end pdf_controller



























main_controllers.controller('add_pdfs_controller', function($scope, $rootScope, $http, $location) {
 // sm("At pdfs.js add_pdfs_controller");

 navbar_remove_all_active();
 $("#navli_add_pdfs").addClass("active");

 $scope.ui = {};
 $scope.ui.pdf_Owner = LoginEmail;
 // $scope.uidirty = false;


// et_name
// et_created_by
// et_updated_by
// et_product_name
// et_website_name  (dropdown)
// et_subject
// et_from_address  
// et_smtp_id   (dropdown)
// et_body


 $scope.rs = $rootScope;

 $scope.$watch("ui",function() {
    var custom_valid = validate_form($scope, $rootScope);
  },true);

  // Cancel adding a new pdf
  $scope.pdf_add_cancel = function () {
    $location.path('/landing_authed');
  };


  //  - clear all fileds except the  owner pdf
  $scope.pdf_add_clear = function () {
    $scope.ui.et_name = '';
    //$scope.ui.et_created_by = '';// we keep the owner along with his _id
    // $scope.ui.et_updated_by = '';
    $scope.ui.et_product_name = '';
    $scope.ui.et_website_name = '';//  (dropdown)
    $scope.ui.et_subject = '';
    $scope.ui.et_from_address = '';  
    $scope.ui.et_smtp_id = '';  // (dropdown)
    $scope.ui.et_body = '';
    $location.path('/pdf_add');
  };

  /*
   Create a test set for quick population of the fields within the document
   */
  $scope.pdf_add_populate = function () {
    // Generate a random number to append to the data
    var rn = Math.floor((Math.random() *1000) + 1);
    var rns = rn.toString();
    $scope.ui.et_name = 'pdf_Name-' + rns;
    // $scope.ui.et_created_by = LoginEmail;
    // $scope.ui.et_updated_by = 'updated by ' + rns;
    $scope.ui.et_product_name = 'product_name' + rns;
    $scope.ui.et_website_name = 'website_name' + rns; //  (dropdown)
    $scope.ui.et_subject = 'subject' + rns;
    $scope.ui.et_from_address = 'from_address' + rns; 
    $scope.ui.et_smtp_id = 'smtp_id' + rns;  // (dropdown)
    $scope.ui.et_body = 'body' + rns;
    $location.path('/pdf_add');
  };


  $scope.pdf_add_submit = function (formvalid) {
    if (!formvalid) {
      $scope.uidirty = true;
      alert('Please check, some entries are not valid. ');
      return;
    }
    // if (!validate_form($scope, $rootScope)) {
    //     $scope.uidirty = true;
    //     alert('Please check, some entries are invalid.');
    //     return;
    //   }

    var payload = {};
      payload.et_name = $scope.ui.et_name;
      // payload.et_created_by = $scope.ui.et_created_by;
      // payload.et_updated_by = $scope.ui.et_updated_by;
      payload.et_product_name = $scope.ui.et_product_name;
      payload.et_website_name = $scope.ui.et_website_name; //  (dropdown)
      payload.et_subject = $scope.ui.et_subject;
      payload.et_from_address = $scope.ui.et_from_address; 
      payload.et_smtp_id = $scope.ui.et_smtp_id;  // (dropdown)
      payload.et_body = $scope.ui.et_body;
      payload.et_created_by = $scope.rs.session.person_id;

    var mypromise =  $http({
      method: 'POST',
        url: $rootScope.api_mba + '/pdfs',   //map to API server.js: app.post('/pdfs', pdfs.pdf_add_one); 
        params: { 'foobar': new Date().getTime() },
        data: payload
      });

    mypromise.success(function(data, status, headers, config) {
      if (status == 201) {
        $location.path('/pdf_add_confirmation');
      } else {
        http_error(data);
      }
    });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });

    function http_error(data){
      if (data.message) {
        alert(">>data.message= " + data.message);
      } else {
        alert('Unknown error in pdfs.js errID-425: ' + data);
      }
    }
  };

  /*

  */
  function validate_form($scope, $rootScope){
    var validate_ok = true;
    $scope.ui.invalid = {};
    
    if (!$scope.ui.accept_tac) {
      $scope.ui.invalid.accept_tac = true;
      validate_ok = false;
    }
    return validate_ok;
  }
});































/*

*/
main_controllers.controller('edit_pdf_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_pdfs").addClass("active");

  $scope.uidirty = false;
  $scope.ui = {};
  //$scope.uilookup = {};

  get_data();


  /*

  */
  function get_data(){
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/pdfs/' + $routeParams.id, //map to API server.js: app.get('/pdfs', pdfs.pdfs_get_one); 
      params: { 'foobar': new Date().getTime() }
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
      } else {
        get_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      get_error(data);
    });
    function get_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in pdf_ pdfs.js (~364): Data:' + data + "  Status:" + status + "  Headers:" + "  Config:" + config);
      }
    }
  }

  $scope.pdf_edit_cancel = function () {

  // show_image('images/wait.gif', 30, 30, 'Wait');
  // sm("at pdf_edit_cancel");
  $location.path('/pdfs');
};

  //  - clear all fields except the  owner pdf
  $scope.pdf_edit_clear = function () {

    $scope.ui.pdf_Name = '';
    // $scope.ui.apiOwner = '';  // we keep the owner along with his _id
    $scope.ui.pdf_Url = '';
    $scope.ui.pdf_Cat = '';
    $scope.ui.pdf_Repo = '';
    $scope.ui.apiRepo = '';
    $scope.ui.pdf_SvrAdr = '';
    $scope.ui.apiSvrAdr = '';
    $scope.ui.apiPort = '';
  };

  $scope.pdf_edit_submit = function (formvalid) {
    /*
    // The following procedure was commented out for some reason
   //  if (!formvalid) {
   //   $scope.uidirty = true;
   //   alert("uidirty= " + uidirty)
   //   return;
   // }
   */

   var mypromise =  $http({
    method: 'PUT',
      url: $rootScope.api_mba + '/pdfs/' + $routeParams.id, //map to API server.js: app.put('/pdfs', pdfs.pdfs_update_one); 
      params: { 'foobar': new Date().getTime() },
      data: $scope.ui
    });

// sm(Object.keys($routeParams));

mypromise.success(function(data, status, headers, config) {
  console.log('pdf update success ',status);
  if (status == 200) {
    alert("pdf template updated successfully");

    $location.path('/pdfs');



  } else {
    update_error(data);
  }
});

mypromise.error(function(data, status, headers, config) {
  update_error(data);
});
function update_error(data){
  if (data.message) {
    alert(data.message);
  } else {
    alert('Unknown error in pdfs.js errID-548: ' + data);
  }
}
};
});

function show_image(src, width, height, alt) {
  var img = document.createElement("img");
  img.src = src;
  img.width = width;
  img.height = height;
  img.alt = alt;

    // This next line will just add it to the <body> tag
    document.body.appendChild(img);
  }