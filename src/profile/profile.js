/*  -------  profile.js  ----------
          
*/
main_controllers.controller('profile_controller', 
  function($http, $scope, $rootScope, $location, $routeParams) {
  // $scope.rs = $rootScope;
  navbar_remove_all_active();
  $("#navli_profile").addClass("active");

  $scope.uidirty = false;
  $scope.ui = {};



  get_data();
  function get_data(){
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/profile/' + $routeParams.id, //map to API server.js: app.get('/websites', websites.websites_get_one); 
      params: { 'foobar': new Date().getTime() }
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
        console.log($scope.ui);
      } else {
        get_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      get_error(data);
    });
    function get_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in profile.js (~30) ' + data);
      }
    }
  }



  // $scope.profile_button = function (pid) {
  //   sm("At profile.js  profile_button    pid=" + pid);
  //   $location.path('/profile_edit');
  // };


  $scope.ui_edit_profile_button = function(){
    sm("At profile.js rs.session.person_id = " + rs.session.person_id);


    if (rs.session.person_id){
      $location.path('/profile_edit/');
    } else {
      sm("Going nowhere");
    }
  };








  $scope.ui_edit_profile_submit = function (formvalid) {
    //if (!formvalid) {
    //  $scope.uidirty = true;
    //  return;
    //}

    var mypromise =  $http({
      method: 'PUT',
      url: $rootScope.api_mba + '/profile/' + $routeParams.id, //map to API server.js: app.put('/websites', websites.websites_update_one); 
      params: { 'foobar': new Date().getTime() },
      data: $scope.ui
    });
    mypromise.success(function(data, status, headers, config) {
      console.log('Profile update success ',status);
      if (status == 200) {
        alert("Profile updated successfully");
      } else {
        update_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      update_error(data);
    });
    function update_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in profile.js (~63) ' + data);
      }
    }
  };
});

