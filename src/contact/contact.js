// ------ contact.js ---------
// api_mba
 
main_controllers.controller('contact_controller', function($scope, $rootScope, $http, $location) {
  navbar_remove_all_active();
  $("#navli_contact").addClass("active");

  $scope.ui = {};
  $scope.uidirty = false;

  $scope.rs = $rootScope;

  $scope.$watch("ui",function() {
    var custom_valid = validate_form($scope, $rootScope);
  },true);

  $scope.ui.email=LoginEmail;
  $scope.ui.contact_copy=true; // We set the state of the checkbox here in the controller and not in the view

// Returning from sending an email
$scope.contact_cancel_btn = function(){
  // sm("At contact.js contact_cancel_btn");
  $location.path('/landing');
  };

// clear all input fields on the contact form
$scope.contact_clear_btn = function(){  
  $scope.ui.contactname='';
  $scope.ui.email='';

  $scope.ui.email=LoginEmail;

  $scope.ui.subject='';

  $scope.ui.message='';
  $location.path('/contact');  // reload the contact form
};

  /*
   Create a test set for quick population of the fields within the document
   */
   $scope.contact_populate_btn = function () {

// Generate a random number to append to the data
var rn = Math.floor((Math.random() *100) + 1);
var rns = rn.toString();

$scope.ui.contactname = 'Contactname ' + rns;
$scope.ui.email = LoginEmail;
 // "contact' + rns + '@name.com';
$scope.ui.subject = 'Subject ' + rns;

$scope.ui.message = 'This is test message - ' + rns;
$scope.ui.contact_copy=true;
$location.path('/contact');
};



// Submit the contact form
    $scope.contact_submit_btn = function (formvalid) {
        // sm("At contact.js contact_submit_btn");
        // sm("formvalid="+formvalid);

    if (!formvalid) {
      $scope.uidirty = true;
      alert('Please check, some entries are not valid.');
      return;
    }
    if (!validate_form($scope, $rootScope)) {
      $scope.uidirty = true;
      alert('Please check, some entries are invalid.');
      return;
    }
    var d = new Date();
    var payload = {};
    payload.datum = d.toDateString()+ ' at ' + d.toTimeString();
    payload.contactname = $scope.ui.contactname;
    payload.email = $scope.ui.email;
    payload.subject = $scope.ui.subject;
    payload.message = $scope.ui.message;
    payload.contact_copy = $scope.ui.contact_copy;
// sm("");
    var mypromise =  $http({
      method: 'POST',
      url: $rootScope.api_mba + '/contact',
      params: { 'foobar': new Date().getTime(), 'parm1':'empty1' },
      data: payload
    });
    mypromise.success(function(data, status, headers, config) {
      // sm("At contact.js mypromise success: status=" + status);
      if (status == 201) {
        $location.path('/contact_confirmation');
      } else {
        http_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      // sm("At contact.js mypromise error: status=" + status);

      http_error(data);
    });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in contact.js ErrID-con100:\nCheck the server. ' + data);
      }
    }
  };

  function validate_form($scope, $rootScope){
    var validate_ok = true;
    $scope.ui.invalid = {};
    
    // if (!$scope.ui.accept_tac) {
    //   $scope.ui.invalid.accept_tac = true;
    //   validate_ok = false;
    // }
    return validate_ok;
  }
});
