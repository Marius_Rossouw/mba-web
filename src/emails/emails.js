/* ---------- emails.js ----------
       
This is the controller that controls the functions within the emails.html file
These functions include 
- ui_get_emails_button()
- view_email
- delete_email
- edit_email_icon

The sequence of variables are as follows for consistency:

et_name
et_created_by
et_updated_by
et_product_name
et_website_name  (dropdown)
et_subject
et_from_address  
et_smtp_id   (dropdown)
et_body



*/
main_controllers.controller('emails_controller', function($http, $scope, $rootScope, $location) {
  navbar_remove_all_active();
  $("#navli_emails").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};

  if (!$scope.rs.ui_emails){
    $scope.rs.ui_emails = {};
    $scope.rs.ui_emails.list = [];
  }

  $scope.ui.email_Owner= LoginEmail;

  /* --------------------------------------------------------------------
  A function that uses GET and links to the backend 
  with url: $rootScope.api_mba + '/emails', and 
  then places the recieved data into the rootScope 
  */
  $scope.ui_get_emails_button = function () {

// sm("LoginEmail=" +  LoginEmail +" " + $rootScope.su);

var mypromise =  $http({
  method: 'GET',
        //map to API server.js: app.get('/emails', emails.emails_list);
        url: $rootScope.api_mba + '/emails',    
        params: {
          'foobar': new Date().getTime(),
          'spr': $rootScope.su,
          'em': LoginEmail
        },
      });
mypromise.success(function(data, status, headers, config) {

  if (status == 200) {
    if(data == "" || data == null || data== undefined){
      alert("No emails found for \n" + LoginEmail);
      return;
    }


    /* place the data into the rootScope(rs) */
    $scope.rs.ui_emails.list = data;         
  } else {
    http_error(data);
  }
});
mypromise.error(function(data, status, headers, config) {
  /* sm("error Object.keys=" + Object.keys(data)); */

  http_error(data);
});
function http_error(data){
  if (data.message) {
    alert(data.message);
  } else {
    alert('Unknown error in email_ emails.js\n errID-80): ' + data);
  }
}
};

//create the arrow button that opens the individual email
var grid_view_email_template = '<div>';
grid_view_email_template += '<button type="button" class="btn btn-xs"';
grid_view_email_template += 'title="View the details of this email"';
grid_view_email_template += 'ng-click="view_email(row.entity[col.field])">';  
// bevat die _id, word gebruik as pid 
// e.g. $scope.view_email = function(pid){
  grid_view_email_template += '<i class="fa fa-arrow-right"></i>';
  grid_view_email_template += '</button>';
  grid_view_email_template += '</div>';

  //create the bin button that deletes the individual email
  var grid_delete_email_template = '<div>';
  grid_delete_email_template += '<button type="button" class="btn btn-xs" ';
  grid_delete_email_template += ' title="Delete this email from the email database" ';
  grid_delete_email_template += 'ng-click="delete_email(row.entity[col.field])">';
  grid_delete_email_template += '<i class="fa fa-trash-o"></i>';
  grid_delete_email_template += '</button>';
  grid_delete_email_template += '</div>';

  var grid_edit_email_template = '<div>';
  grid_edit_email_template += '<button type="button" class="btn btn-xs" ';
  grid_edit_email_template += 'title="Edit the details of this email" ';
  grid_edit_email_template += 'ng-click="edit_email_icon(row.entity[col.field])">';
  grid_edit_email_template += '<i class="fa fa-pencil"></i>';
  grid_edit_email_template += '</button>';
  grid_edit_email_template += '</div>';
  
  /*
  A function that redirects the browser to view the selected email
  */
  $scope.view_email = function(pid){  
   // the pid is derived from the definition 
  // of the grid_view_template above

  $location.path('/email/' + pid );
};

  /*
  A function that redirects the browser to edit the selected email
  */
  $scope.edit_email_icon = function(pid){
  // console.log(pid);
  $location.path('/email_edit/' + pid);
};

//builds the grid within which the emails will appear as a list
$scope.grid_cols_def = [
{field: '_id', displayName: 'Open', width: 50, 
cellTemplate : grid_view_email_template, 
editableCellTemplate : grid_view_email_template },
{field: '_id', displayName: 'Del', width: 35, 
cellTemplate : grid_delete_email_template, 
editableCellTemplate : grid_delete_email_template },
{field: '_id', displayName: 'Edit', width: 40, 
cellTemplate : grid_edit_email_template, 
editableCellTemplate : grid_edit_email_template },
{field: 'et_name', displayName: 'Email name', width: 130},
{field: 'et_created_by', displayName: 'Email creator', width: 100} ,
{field: 'et_updated_by', displayName: 'Email updated by', width: 100} ,
{field: 'et_product_name', displayName: 'Product name', width: 130},
{field: 'et_website_name', displayName: 'Website name', width: 100} ,
{field: 'et_subject', displayName: 'Email subject', width: 150},
{field: 'et_from_address', displayName: 'Email From affress', width: 150},
{field: 'et_smtp_id', displayName: 'E smtp_id', width: 200},
{field: 'et_body', displayName: 'Email body', width: 80}

];

// et_name
// et_created_by
// et_updated_by
// et_product_name
// et_website_name  (dropdown)
// et_subject
// et_from_address  
// et_smtp_id   (dropdown)
// et_body



$scope.grid_filter_options = {
  filterText: ''
};

//Setting for the grid that contains the emails
$scope.grid_options = {
  data: 'rs.ui_emails.list',
  enableCellSelection: false,
  enableRowSelection: false,
  enableCellEditOnFocus: true,
  columnDefs: 'grid_cols_def',
  enablePaging: true,
  filterOptions: $scope.grid_filter_options
};

  /*
  A function that uses DELETE and links to the backend 
  with url: $rootScope.api_mba + '/emails/' + pid, and 
  after deleting the selected email it recalls the 
  ui_get_emails_button() function to reload the list of 
  emails in the grid
  */
  $scope.delete_email = function(pid){

    if(confirm("Are you sure you want to delete this email?") == false) {
      return;
    }


    var mypromise =  $http({
      method: 'DELETE',
        url: $rootScope.api_mba + '/emails/' + pid,    //map to API server.js: app.delete('/emails/:id', emails.emails_delete_one);
        params: {
          'foobar': new Date().getTime()
        }
      });

    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
          $scope.ui_get_emails_button();       // Recalls the ui_get_emails_button() function to reload the list of scchools in the grid
        } else {
          http_error(data);
        }
      });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });

    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in email_ emails.js \nerrID-207: ' + data);
      }
    }
  };
});


/*

*/
main_controllers.controller('email_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_emails").addClass("active");
  $scope.rs = $rootScope;
  $scope.ui = {};

  /*

  */
  $scope.ui_get_email_button = function () {
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/emails/' + $routeParams.id,
      params: {
        'foobar': new Date().getTime()
      }

    });



    mypromise.success(function(data, status, headers, config) {
        // sm("success Object.keys=" + Object.keys(data));

        if (status == 200) {


          $scope.ui = data;
        } else {
          http_error(data);
        }
      });

    mypromise.error(function(data, status, headers, config) {

        // sm("error1 Object.keys=" + Object.keys(data));


        http_error(data);
      });
    function http_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error  in  emails.js errID-274: ' + data);
      }
    }
  };

  $scope.ui_get_email_button();

  /*

  */
  $scope.ui_edit_email_button = function(){

    if ($scope.ui._id){
      $location.path('/email_edit/' + $scope.ui._id);
    }
  };


  $scope.email_view_cancel = function () {
    // sm("at email_view_cancel");
    $location.path('/emails');
  };

});    // end email_controller



























main_controllers.controller('add_emails_controller', function($scope, $rootScope, $http, $location) {
 // sm("At emails.js add_emails_controller");

 navbar_remove_all_active();
 $("#navli_add_emails").addClass("active");

 $scope.ui = {};
 $scope.ui.email_Owner = LoginEmail;
 // $scope.uidirty = false;


// et_name
// et_created_by
// et_updated_by
// et_product_name
// et_website_name  (dropdown)
// et_subject
// et_from_address  
// et_smtp_id   (dropdown)
// et_body


 $scope.rs = $rootScope;

 $scope.$watch("ui",function() {
    var custom_valid = validate_form($scope, $rootScope);
  },true);

  // Cancel adding a new email
  $scope.email_add_cancel = function () {
    $location.path('/landing_authed');
  };


  //  - clear all fileds except the  owner email
  $scope.email_add_clear = function () {
    $scope.ui.et_name = '';
    //$scope.ui.et_created_by = '';// we keep the owner along with his _id
    // $scope.ui.et_updated_by = '';
    $scope.ui.et_product_name = '';
    $scope.ui.et_website_name = '';//  (dropdown)
    $scope.ui.et_subject = '';
    $scope.ui.et_from_address = '';  
    $scope.ui.et_smtp_id = '';  // (dropdown)
    $scope.ui.et_body = '';
    $location.path('/email_add');
  };

  /*
   Create a test set for quick population of the fields within the document
   */
  $scope.email_add_populate = function () {
    // Generate a random number to append to the data
    var rn = Math.floor((Math.random() *1000) + 1);
    var rns = rn.toString();
    $scope.ui.et_name = 'email_Name-' + rns;
    // $scope.ui.et_created_by = LoginEmail;
    // $scope.ui.et_updated_by = 'updated by ' + rns;
    $scope.ui.et_product_name = 'product_name' + rns;
    $scope.ui.et_website_name = 'website_name' + rns; //  (dropdown)
    $scope.ui.et_subject = 'subject' + rns;
    $scope.ui.et_from_address = 'from_address' + rns; 
    $scope.ui.et_smtp_id = 'smtp_id' + rns;  // (dropdown)
    $scope.ui.et_body = 'body' + rns;
    $location.path('/email_add');
  };


  $scope.email_add_submit = function (formvalid) {
    if (!formvalid) {
      $scope.uidirty = true;
      alert('Please check, some entries are not valid. ');
      return;
    }
    // if (!validate_form($scope, $rootScope)) {
    //     $scope.uidirty = true;
    //     alert('Please check, some entries are invalid.');
    //     return;
    //   }

    var payload = {};
      payload.et_name = $scope.ui.et_name;
      // payload.et_created_by = $scope.ui.et_created_by;
      // payload.et_updated_by = $scope.ui.et_updated_by;
      payload.et_product_name = $scope.ui.et_product_name;
      payload.et_website_name = $scope.ui.et_website_name; //  (dropdown)
      payload.et_subject = $scope.ui.et_subject;
      payload.et_from_address = $scope.ui.et_from_address; 
      payload.et_smtp_id = $scope.ui.et_smtp_id;  // (dropdown)
      payload.et_body = $scope.ui.et_body;

    var mypromise =  $http({
      method: 'POST',
        url: $rootScope.api_mba + '/emails',   //map to API server.js: app.post('/emails', emails.email_add_one); 
        params: { 'foobar': new Date().getTime() },
        data: payload
      });

    mypromise.success(function(data, status, headers, config) {
      if (status == 201) {
        $location.path('/email_add_confirmation');
      } else {
        http_error(data);
      }
    });

    mypromise.error(function(data, status, headers, config) {
      http_error(data);
    });

    function http_error(data){
      if (data.message) {
        alert(">>data.message= " + data.message);
      } else {
        alert('Unknown error in emails.js errID-425: ' + data);
      }
    }
  };

  /*

  */
  function validate_form($scope, $rootScope){
    var validate_ok = true;
    $scope.ui.invalid = {};
    
    if (!$scope.ui.accept_tac) {
      $scope.ui.invalid.accept_tac = true;
      validate_ok = false;
    }
    return validate_ok;
  }
});































/*

*/
main_controllers.controller('edit_email_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_emails").addClass("active");

  $scope.uidirty = false;
  $scope.ui = {};
  //$scope.uilookup = {};

  get_data();


  /*

  */
  function get_data(){
    var mypromise =  $http({
      method: 'GET',
      url: $rootScope.api_mba + '/emails/' + $routeParams.id, //map to API server.js: app.get('/emails', emails.emails_get_one); 
      params: { 'foobar': new Date().getTime() }
    });
    mypromise.success(function(data, status, headers, config) {
      if (status == 200) {
        $scope.ui = data;
      } else {
        get_error(data);
      }
    });
    mypromise.error(function(data, status, headers, config) {
      get_error(data);
    });
    function get_error(data){
      if (data.message) {
        alert(data.message);
      } else {
        alert('Unknown error in email_ emails.js (~364): Data:' + data + "  Status:" + status + "  Headers:" + "  Config:" + config);
      }
    }
  }

  $scope.email_edit_cancel = function () {

  // show_image('images/wait.gif', 30, 30, 'Wait');
  // sm("at email_edit_cancel");
  $location.path('/emails');
};

  //  - clear all fields except the  owner email
  $scope.email_edit_clear = function () {

    $scope.ui.email_Name = '';
    // $scope.ui.apiOwner = '';  // we keep the owner along with his _id
    $scope.ui.email_Url = '';
    $scope.ui.email_Cat = '';
    $scope.ui.email_Repo = '';
    $scope.ui.apiRepo = '';
    $scope.ui.email_SvrAdr = '';
    $scope.ui.apiSvrAdr = '';
    $scope.ui.apiPort = '';
  };

  $scope.email_edit_submit = function (formvalid) {
    /*
    // The following procedure was commented out for some reason
   //  if (!formvalid) {
   //   $scope.uidirty = true;
   //   alert("uidirty= " + uidirty)
   //   return;
   // }
   */

   var mypromise =  $http({
    method: 'PUT',
      url: $rootScope.api_mba + '/emails/' + $routeParams.id, //map to API server.js: app.put('/emails', emails.emails_update_one); 
      params: { 'foobar': new Date().getTime() },
      data: $scope.ui
    });

// sm(Object.keys($routeParams));

mypromise.success(function(data, status, headers, config) {
  console.log('email update success ',status);
  if (status == 200) {
    alert("Email template updated successfully");

    $location.path('/emails');



  } else {
    update_error(data);
  }
});

mypromise.error(function(data, status, headers, config) {
  update_error(data);
});
function update_error(data){
  if (data.message) {
    alert(data.message);
  } else {
    alert('Unknown error in emails.js errID-548: ' + data);
  }
}
};
});

function show_image(src, width, height, alt) {
  var img = document.createElement("img");
  img.src = src;
  img.width = width;
  img.height = height;
  img.alt = alt;

    // This next line will just add it to the <body> tag
    document.body.appendChild(img);
  }